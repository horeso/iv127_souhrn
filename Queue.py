class Node:

	def __init__(self, value=None, prev=None):
		self.value = value
		self.prev = prev

class Queue:

	def __init__(self):
		self.first = None
		self.last = None

	def enqueue(self, value):
		new_node = Node(value)

		if self.first is None:
			self.last = new_node
			self.first = self.last

		else:
			self.last.prev = new_node
			self.last = new_node

	def dequeue(self):
		answer = None

		if self.first:
			answer = self.first.value
			self.first = self.first.prev

			if self.first is None:
				self.last = None

		return answer

	def is_empty(self):
		return self.first is None

	def __str__(self):
		if self.first is None:
			return 'queue empty'

		s = 'first <- '

		tmp = self.first
		while tmp:
			s += str(tmp.value) + ' <- '
			tmp = tmp.prev
		s += 'last'

		return s