import pandas as pd
from os import listdir
from os.path import isfile, join


## load dataset
def load_dataset(data_folder):
    """
    Loads all .csv files from data_folder to a dictionary
    key = filename, value = dataframe
    """
    onlyfiles = [f for f in listdir(data_folder) if isfile(join(data_folder, f))]
    dfs = {}
    names = [s.split('.')[0] for s in onlyfiles if s.split('.')[1] == 'csv']
    for name in names:
        filepath = data_folder + name + '.csv'
        dfs[name] = pd.read_csv(filepath)
    return dfs

## processing task
# only counts with executions of programs

def prepare_task(task_number, snapshots, tasks, execution_only=True):
    """
    Returns joined snapshots and tasks together,
    NaN programs are not included,
    sorted by time
    """
    merged = pd.merge(snapshots, tasks, left_on='task_session', right_on='id')
    merged = merged[pd.notnull(merged['program'])]
    if execution_only:
        executions = merged.loc[merged['granularity'] == 'execution']
    else:
        executions = merged
        
    cur_task = executions.loc[executions['task'] == task_number]
    cur_task['time'] = pd.to_datetime(cur_task['time'])
    sorted_task = cur_task.sort_values(by='time')
    return sorted_task
    
def get_prepared_task(task_number, directory):
    data = load_dataset(directory)

    return prepare_task(task_number, data['program_snapshots'], 
                        data['task_sessions'], False)

def get_students(df):
    """
    Returns list of unique student numbers
    """
    return list(set(df['student'].values))

def get_task_number(task_name, data):
    """
    Input:
    - task_name: string
    - data: dictionary of dataframes
    
    Returns id number of given task
    """
    tasks = data['tasks']
    return tasks.loc[tasks['name']==task_name]['id'].iloc[0]

def get_task_name(task_number, data):
    tasks = data['tasks']
    if not task_number in data['tasks']['id']:
        return None

    right_number = tasks.loc[tasks['id']==task_number] 
    return right_number['name'].iloc[0]

# https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
def levenshtein(s1, s2):
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]

def get_distances(prog_list, dist_func):
    """
    Input:
    - prog_list: list of strings
    - dist_func: takes two strings as input and returns their distance
    
    Returns dictionary indexed by string pairs, values are their distances
    """
    dists = {}
    for s1 in prog_list:
        for s2 in prog_list:
            dist = dist_func(s1, s2)
            dists[s1, s2] = dist
            
    return dists