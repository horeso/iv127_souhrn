class Node:
	"""
	Node for stack implementation
	"""
	def __init__(self, value=None, prev=None, next=None):
		self.value = value
		self.next = next

class Stack:

	def __init__(self):
		self.top = None
		self.length = 0

	def push(self, value):
		new_node = Node(value)

		if self.top:
			new_node.next = self.top

		self.top = new_node
		self.length += 1

	def pop(self):
		"""
		Pops value from the stack, if stack is empty returns None
		"""
		answer = None
		if self.top:
			answer = self.top.value
			self.top = self.top.next
			self.length -= 1

		return answer

	def get_length(self):
		return self.length

	def is_empty(self):
		return self.top is None

	def get_top(self):
		if self.top:
			return self.top.value
		else:
			return None
			
	def __str__(self):
		if self.length == 0:
			return 'stack is empty'

		s = 'top'

		tmp = self.top
		while tmp:
			s += ' -> ' + str(tmp.value)
			tmp = tmp.next

		return s