import random

from data_utils import get_distances, levenshtein

class KMeans:

    def __init__(self):
        pass

    def init_centroids(self, clusters, prog_list):
        """
        Randomly initializes centroids
        
        Inputs:
        - clusters: int; number of clusters
        - prog_list: list of strings
        """
        prog_len = len(prog_list)
        centroids = []

        indices = []
        for i in range(clusters):

            tmp = random.randint(0, prog_len - 1)
            while tmp in indices:
                tmp = random.randint(0, prog_len - 1)
            indices.append(tmp)

        return [prog_list[i] for i in indices]

    # find new centroid
    def min_average(self, alist, dists):
        """
        Finds member of alist which has lowest average distance to others
        """
        min_id = alist[0]
        min_dist = self.average_dist(alist[0], alist, dists)
        
        for i in range(1, len(alist)):
            dist = self.average_dist(alist[i], alist, dists)
            if dist < min_dist:
                min_dist = dist
                min_id = alist[i]
                
        return min_id, min_dist
        
    def average_dist(self, prog, alist, dists):
        """
        Computes average distances
        """
        if len(alist) == 0:
    #         return 10e6
            return 0
        return sum([dists[prog, x] for x in alist]) / len(alist)

    def assign_clusters(self, centroids, prog_list, dists):
        prog_len = len(prog_list)
        cluster_id = ['']*prog_len
        for i in range(len(prog_list)):
            prog = prog_list[i]
            min_dist = dists[centroids[0], prog]
            min_id = centroids[0]
            for centr in centroids:
                dist = dists[centr, prog]
                if dist < min_dist:
                    min_dist = dist
                    min_id = centr
            cluster_id[i] = min_id
        return cluster_id
        
    def get_new_centroids(self, centroids, prog_list, cluster_id, dists):
        clusters = len(centroids)
        prog_len = len(prog_list)
        new_centroids = []
        for i in range(clusters):
            # programs from this cluster
            cur_progs = [prog_list[j] for j in range(prog_len) if centroids[i] == cluster_id[j]]
    #         print(cur_progs)
            min_id, min_dist = self.min_average(cur_progs, dists)
    #         print(min_id, min_dist)
    #         min_id = list_mode(cur_progs)
            new_centroids.append(min_id)
            
        return new_centroids


    # works with averages
    def train(self, prog_list, clusters, dists, verbose=False):
        centroids = self.init_centroids(clusters, prog_list)
        old_centroids = []
        while centroids != old_centroids:
            old_centroids = centroids
            cluster_ids = self.assign_clusters(centroids, prog_list, dists)
            centroids = self.get_new_centroids(centroids, prog_list, cluster_ids, dists)
            
        # results of clustering
        cluster_ids = self.assign_clusters(centroids, prog_list, dists)
        if verbose:
            print('Final distances and centroids:')
        total_dist = 0
        prog_len = len(prog_list)
        for i in range(clusters):
            cur_progs = [prog_list[j] for j in range(prog_len) if centroids[i] == cluster_ids[j]]
    #         print(cur_progs)
            dist = self.average_dist(centroids[i], cur_progs, dists)
            if verbose:
                print(centroids[i], dist, cur_progs)
            total_dist += dist
        
        if verbose:
            print('Total distance:', total_dist)
        return centroids, total_dist


    def fit(self, clusters, epochs, false_prog_list):
        dists = get_distances(false_prog_list, levenshtein)
        res = self.train(false_prog_list, clusters, dists, False)
        
        min_centroids = res[0]
        min_dist = res[1]
        for i in range(1000):
            res = self.train(false_prog_list, clusters, dists, False)
            centrs = res[0]
            dist = res[1]
            if dist < min_dist:
                min_dist = dist
                min_centroids = centrs

        return min_centroids, min_dist