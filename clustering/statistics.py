import matplotlib.pyplot as plt

from data_utils import load_dataset, get_task_name, prepare_task

def tasks_solution_hist(data_folder, verbose=False):
    ys = list(task_solution_count(data_folder, verbose).values())
        
    plt.hist(ys)
    plt.show()

def task_solution_count(data_folder, verbose=False):
    data = load_dataset(data_folder)
    snapshots = data['program_snapshots']
    tasks = data['task_sessions']
    
    result = {}
    i = 1
    while get_task_name(i, data):
        sorted_task = prepare_task(i, snapshots, tasks, False)
        rows = len(sorted_task.index)
        result[i] = rows
        i += 1
        
        if verbose:
            print('Task ', i, ':', rows, 'rows')

    return result