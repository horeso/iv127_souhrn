"""
Utilities for working with abstract syntax trees
"""

from zss import simple_distance, Node

# for faster distance computation
from multiprocessing import Pool

def label_dist(a, b):
    adv = ['W', 'R', 'I']
    if a == '' and b == '':
        return 0
    if a == '':
        return 1
    if b == '':
        return 1
    if a[0] in adv and b[0] in adv:
        if a[0] != b[0]:
            return 2
        if a == b:
            return 0
        return 1
    if a == b:
        return 0
    return 1

def simple_label_distance(a, b):
    return simple_distance(a, b, label_dist=label_dist)

def program_to_tree(program):
    root = Node('start')
    return _program_to_tree(program, root)

def _program_to_tree(program, root):
    """
    Recursive version
    
    Input:
    - program: string, valid robomission program
    """
    if program == '':
        return root
    
    length = len(program)
    
    run = True
    i = 0
    while run:
        current_char = program[i]

        # case when we find R, W, I
        if current_char.isupper():
            label = ""
            while current_char != '{':
                label += current_char
                i += 1
                current_char = program[i]
                
            node = Node(label)
            root.addkid(node)
            
            first_block, last_bracket_index = get_subcommand(program[i:])
            
            # If has up to two kids, 'if' and 'else'
            if label[0] == 'I':
                if_node = Node('if')
                node.addkid(if_node)
                _program_to_tree(first_block, if_node)

                slash_index = i + last_bracket_index
                # case of else block
                if slash_index < length and program[slash_index] == '/':
                    second_block, new_last_brack_ind = get_subcommand(program[slash_index+1:])
                    else_node = Node('else')
                    node.addkid(else_node)
                    _program_to_tree(second_block, else_node)
                    i = new_last_brack_ind + slash_index
                    
                else:
                    i = last_bracket_index + i - 1
                    
            else:
                _program_to_tree(first_block, node)
                i = last_bracket_index + i - 1
            
        # commands: f, l, r, s
        elif current_char in 'flrs':
            node = Node(current_char)
            root.addkid(node)
            
        i += 1
        
        if i >= length:
            run = False
            break
                
    return root 

def get_subcommand(string):
    """
    Input: string in form: '{to_return}...'
    Returns 'to_return' part of string, index to one char after last }
    """
    
    
    length = len(string)
    to_return = ""
    opened = 1
        
    for i in range(1, length):
        current_char = string[i]
        if current_char == '{':
            opened += 1
        elif current_char == '}':
            opened -= 1
            
        if current_char == '}' and opened == 0:
            return to_return, i + 1
        
        to_return += current_char

def print_tree(tree, level = 0):
    if level > 0:
        print(' ' * (4 * (level-1) + 1), '-' * 3, tree.label)
    else:
        print(' '*4*level, tree.label)
    for sub_tree in tree.children:
        print_tree(sub_tree, level + 1)

# def get_ast_distances(programs):
#     ast_program_tuples = [(program, program_to_tree(program, Node('root'))) for program in programs]
#     print('Totally we have', len(programs), 'programs')
#     dists = {}
#     for prog1, ast1 in ast_program_tuples:
#         for prog2, ast2 in ast_program_tuples:
#             dists[prog1, prog2] = simple_distance(ast1, ast2)

#         print('Distances computed for:', prog1)

#     return dists

# multithreaded version
def get_ast_distances(programs):
    ast_program_tuples = [(program, program_to_tree(program, Node('root'))) for program in programs]
    print('Totally we have', len(programs), 'programs')

    p = Pool(10)
    inputs = []
    
    for prog1, ast1 in ast_program_tuples:
        for prog2, ast2 in ast_program_tuples:
            inputs.append((prog1, ast1, prog2, ast2))

    dists_tupled = p.map(compute_distance, inputs)

    dists = {}
    for prog1, prog2, dist in dists_tupled:
        dists[prog1, prog2] = dist
        
    return dists

def compute_distance(four_tuple):
    prog1, ast1, prog2, ast2 = four_tuple

    dist = simple_distance(ast1, ast2)

    return (prog1, prog2, dist)