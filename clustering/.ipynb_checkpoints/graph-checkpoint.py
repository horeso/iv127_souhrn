import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import pylab

G = nx.DiGraph()

graph = {
 ('correct', 'end'): 47,
 ('ff', 'end'): 1,
 ('ff', 'ff'): 119,
 ('ff', 'fffrfs'): 9,
 ('ff', 'ls'): 10,
 ('ff', 'lsr'): 2,
 ('ff', 'lsrflf'): 8,
 ('ff', 'rfffll'): 47,
 ('ff', 'rffflss'): 7,
 ('ff', 'rss'): 24,
 ('fffrfs', 'end'): 1,
 ('fffrfs', 'ff'): 8,
 ('fffrfs', 'fffrfs'): 42,
 ('fffrfs', 'ls'): 1,
 ('fffrfs', 'lsrflf'): 5,
 ('fffrfs', 'rfffll'): 5,
 ('fffrfs', 'rffflss'): 6,
 ('fffrfs', 'rss'): 1,
 ('ls', 'ff'): 6,
 ('ls', 'ls'): 66,
 ('ls', 'lsr'): 43,
 ('ls', 'lsrflf'): 8,
 ('ls', 'rss'): 11,
 ('lsr', 'correct'): 6,
 ('lsr', 'end'): 2,
 ('lsr', 'ls'): 3,
 ('lsr', 'lsr'): 59,
 ('lsr', 'lsrflf'): 54,
 ('lsr', 'rffflss'): 1,
 ('lsr', 'rss'): 3,
 ('lsrflf', 'correct'): 42,
 ('lsrflf', 'end'): 2,
 ('lsrflf', 'ff'): 1,
 ('lsrflf', 'fffrfs'): 5,
 ('lsrflf', 'ls'): 12,
 ('lsrflf', 'lsr'): 17,
 ('lsrflf', 'lsrflf'): 193,
 ('lsrflf', 'rfffll'): 6,
 ('lsrflf', 'rffflss'): 3,
 ('lsrflf', 'rss'): 7,
 ('rfffll', 'end'): 2,
 ('rfffll', 'ff'): 18,
 ('rfffll', 'fffrfs'): 7,
 ('rfffll', 'ls'): 4,
 ('rfffll', 'lsr'): 1,
 ('rfffll', 'lsrflf'): 4,
 ('rfffll', 'rfffll'): 122,
 ('rfffll', 'rffflss'): 62,
 ('rfffll', 'rss'): 10,
 ('rffflss', 'end'): 7,
 ('rffflss', 'ff'): 14,
 ('rffflss', 'fffrfs'): 5,
 ('rffflss', 'ls'): 11,
 ('rffflss', 'lsr'): 1,
 ('rffflss', 'lsrflf'): 2,
 ('rffflss', 'rfffll'): 36,
 ('rffflss', 'rffflss'): 216,
 ('rffflss', 'rss'): 7,
 ('rss', 'end'): 4,
 ('rss', 'ff'): 41,
 ('rss', 'fffrfs'): 1,
 ('rss', 'ls'): 13,
 ('rss', 'lsr'): 5,
 ('rss', 'lsrflf'): 13,
 ('rss', 'rfffll'): 13,
 ('rss', 'rffflss'): 3,
 ('rss', 'rss'): 161,
 ('start', 'ff'): 20,
 ('start', 'ls'): 14,
 ('start', 'rfffll'): 1,
 ('start', 'rffflss'): 1,
 ('start', 'rss'): 30}

 
threshold = 0.01
suma = sum(graph.values())
print(suma)
graph = {k: v for k, v in graph.items() if v/suma > threshold or k[0]=='start'}

for key, val in graph.items():
 	G.add_edge(key[0], key[1], weight=val)

val_map = {'A': 1.0,
                   'D': 0.5714285714285714,
                              'H': 0.0}

values = [val_map.get(node, 1.0) for node in G.nodes()]
edge_labels=dict([((u,v,),d['weight'])
                 for u,v,d in G.edges(data=True)])
red_edges = [('C','D'),('D','A')]
edge_colors = ['black' if not edge in red_edges else 'red' for edge in G.edges()]

pos = graphviz_layout(G)
nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels, label_pos=0.3)
node_labels = {node:node for node in G.nodes()}
nx.draw_networkx_labels(G, pos, labels=node_labels)
nx.draw(G,pos, node_size=1500,edge_color=edge_colors,edge_cmap=plt.cm.Reds)
pylab.show()