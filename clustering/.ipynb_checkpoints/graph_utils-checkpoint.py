# creates directed weighted graph
def create_graph(student_list, sorted_task):
    """
    Inputs:
    - student_list: list of student ids
    - sorted_task: dataframe with programs sorted by time
    
    Returns graph where nodes are programs and edges are transitions between them
    weight of an edge means how many students moved that way
    """
    graph = {}
    for student in student_list:
        prog_list = list(sorted_task.loc[sorted_task['student'] == student]['program'])
#         prog_list = prog_list_with_correct(prog_list, sorted_task)
#         false_prog_list = list(map(lambda x: x[0], filter(lambda x: not x[1], prog_list)))
#         correct_prog_list = list(map(lambda x: x[0], filter(lambda x: x[1], prog_list)))
        # print(prog_list)
        graph['start', prog_list[0]] = graph.get(('start', prog_list[0]), 0) + 1

        for i in range(len(prog_list)-1):
            cur_prog = prog_list[i]
            next_prog = prog_list[i + 1]

            graph[cur_prog, next_prog] = graph.get((cur_prog, next_prog), 0) + 1
        graph[prog_list[-1], 'end'] = graph.get((prog_list[-1], 'end'), 0) + 1    
        
    return graph

def create_graph_sess(task_session_list, sorted_task):
    """
    Inputs:
    - student_list: list of student ids
    - sorted_task: dataframe with programs sorted by time
    
    Returns graph where nodes are programs and edges are transitions between them
    weight of an edge means how many students moved that way
    """
    graph = {}
    for session in task_session_list:
        prog_list = list(sorted_task.loc[sorted_task['task_session'] == session]['program'])
#         prog_list = prog_list_with_correct(prog_list, sorted_task)
#         false_prog_list = list(map(lambda x: x[0], filter(lambda x: not x[1], prog_list)))
#         correct_prog_list = list(map(lambda x: x[0], filter(lambda x: x[1], prog_list)))
        # print(prog_list)
        graph['start', prog_list[0]] = graph.get(('start', prog_list[0]), 0) + 1

        for i in range(len(prog_list)-1):
            cur_prog = prog_list[i]
            next_prog = prog_list[i + 1]

            graph[cur_prog, next_prog] = graph.get((cur_prog, next_prog), 0) + 1
        graph[prog_list[-1], 'end'] = graph.get((prog_list[-1], 'end'), 0) + 1    
        
    return graph

def create_cluster_graph(graph, cluster_dict, correct_prog_list):
    """
    Clusters nodes if a graph into a new graph

    Input:
    - graph: graph to cluster
    - cluster_dict: key is cluster centroid
    - correct_prog_list: list of programs which are correct
    """
    new_graph = {}
    for key, val in graph.items():
        first = key[0]
        second = key[1]

        if first == 'start':
            new_first = first
        else:
            new_first = ''
        
        if second == 'end':
            new_second = 'end'
            if first in correct_prog_list:
                new_first = 'correct'
        else:
            new_second = ''

        for centroid, cluster_list in cluster_dict.items():
            if new_first != '' and new_second != '':
                break
            if first in cluster_list:
                new_first = centroid
                # print(centroid)
            if second in cluster_list:
                new_second = centroid
                # print(centroid)
            elif second in correct_prog_list:
                new_second = 'correct'
        # debug
        if new_first == '':
            # print('empty first', key, val, cluster_dict)
            new_first = 'correct'
        new_graph[new_first, new_second] = new_graph.get((new_first, new_second), 0) +  val
    return new_graph